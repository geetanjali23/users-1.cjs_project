// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10

const users = require("./data/1-users.cjs");
function sortUsersBasedOnSeniority(users) {
    let seniorityWiseUsers = Object.entries(users).sort((firstUser, secondUser) => {
        if (firstUser[1].desgination.indexOf("Senior") > -1) {
            if (secondUser[1].desgination.indexOf("Senior") > -1) {
                if (firstUser[1].age > secondUser[1].age) {
                    return -1;
                }
            }
            if (secondUser[1].desgination.indexOf("Developer") > -1 && secondUser[1].desgination.indexOf("Senior") == -1) {
                return -1;
            }
            if (secondUser[1].desgination.indexOf("Intern") > -1) {
                return -1;
            }
        }
        if (firstUser[1].desgination.indexOf("Developer") > -1) {
            if (secondUser[1].desgination.indexOf("Developer") > -1 && secondUser[1].desgination.indexOf("Senior") == -1) {
                if (firstUser[1].age > secondUser[1].age) {
                    return -1;
                }
            }
            if (secondUser[1].desgination.indexOf("Intern") > -1) {
                return -1
            }
        }
        else {
            if (secondUser[1].desgination.indexOf("Intern") > -1) {
                return -1
            }
            return 0;
        }
    });
    return seniorityWiseUsers;
}
console.log(sortUsersBasedOnSeniority(users), "Result");