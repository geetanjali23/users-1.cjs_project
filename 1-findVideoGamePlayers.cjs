// Q1 Find all users who are interested in playing video games.
const users = require("./data/1-users.cjs");
console.log(users, "Given users object");
function findVideoGamePlayers(users) {
    let videoGamePlayers = [];
    for (let key in users) {
        let interests = users[key]["interests"];
        if (interests != undefined) {
            if (interests[0].includes("Video Games") || interests[0].includes("Playing Video Games")) {
                videoGamePlayers.push(key);
            }
        }
    }
    return videoGamePlayers;
}
console.log(findVideoGamePlayers(users), "Printing ans");
