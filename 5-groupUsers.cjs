// Q5 Group users based on their Programming language mentioned in their designation.
const users = require("./data/1-users.cjs");
function groupUsersBasedOnProgrammingLanguage(users) {
    let ans = {};
    for (let key in users) {
        let skillType = users[key]["desgination"];
        if (skillType.includes("Javascript")) {
            if (ans["JavaScript"]) {
                ans["JavaScript"].push(key);
            }
            else {
                ans["JavaScript"] = [];
                ans["JavaScript"].push(key);
            }
        }
        else if (skillType.includes("Golang")) {
            if (ans["Golang"]) {
                ans["Golang"].push(key);
            }
            else {
                ans["Golang"] = [];
                ans["Golang"].push(key);
            }
        }
        else {
            if (ans["Python"]) {
                ans["Python"].push(key);
            }
            else {
                ans["Python"] = [];
                ans["Python"].push(key);
            }
        }
    }
    return ans;
}
console.log(groupUsersBasedOnProgrammingLanguage(users), "ans data");