// Q4 Find all users with masters Degree.
const users = require("./data/1-users.cjs");
function mastersDegreeUsers(users) {
    let resObj = {};
    for (let key in users) {
        let degreeType = users[key].qualification;
        if (degreeType != undefined) {
            if (degreeType.includes("Masters")) {
                resObj[key] = users[key];
            }
        }
    }
    return resObj;
}

console.log(mastersDegreeUsers(users), "masters degree users");