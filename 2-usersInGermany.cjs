// Q2 Find all users staying in Germany.
const users = require("./data/1-users.cjs");
function usersInGermany(users) {
    let resObj = {};
    for (let key in users) {
        let country = users[key].nationality;
        if (country != undefined) {
            if (country.includes("Germany")) {
                resObj[key] = users[key];
            }
        }
    }
    return resObj;
}
console.log(usersInGermany(users), "Printing answer");